#!/usr/bin/env bash

apt-get update
#apt-get install -y apache2

# add php7.0
apt-get install python-software-properties software-properties-common -y
LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
apt-get install php7.0 -y
apt-get install php7.0-fpm -y
apt-get install php7.0-mysql -y

# add composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
#php -r "if (hash_file('SHA384', 'composer-setup.php') === '55d6ead61b29c7bdee5cccfb50076874187bd9f21f65d8991d46ec5cc90518f447387fb9f76ebae1fbbacf329e583e30') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

# add git
apt-get install git-all -y

# add parser
php composer.phar require kingsquare/php-mt940

# validate composer 
php composer.phar validate

# status composer
php composer.phar status --verbose



